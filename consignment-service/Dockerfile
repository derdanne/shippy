FROM golang:latest AS builder

ENV PROTOBUF_VER=3.9.1
ENV GOBIN=$GOPATH/bin
ENV PATH=$PATH:$GOBIN
ENV CGO_ENABLED=0 
ENV GOOS=linux 
ENV GOARCH=amd64

# Install gRPC
RUN apt-get update && \
    apt-get -y install git unzip build-essential autoconf libtool wget

RUN wget https://github.com/protocolbuffers/protobuf/releases/download/v${PROTOBUF_VER}/protobuf-cpp-${PROTOBUF_VER}.zip && \
    unzip protobuf-cpp-${PROTOBUF_VER}.zip && \
    cd protobuf-${PROTOBUF_VER} && \
    ./configure && \
    make && \
    make check && \
    make install && \
    ldconfig && \
    make clean

RUN go get -u github.com/golang/protobuf/proto
RUN go get -u github.com/golang/protobuf/protoc-gen-go
RUN go get -u github.com/micro/protoc-gen-micro

RUN mkdir /app 
ADD main.go /app/
ADD proto /app/proto
WORKDIR /app

# Build application
RUN protoc --proto_path=$GOPATH/src:. --micro_out=. --go_out=plugins=micro:. proto/consignment/consignment.proto
RUN go get
RUN go build -a -tags netgo -ldflags '-w -extldflags "-static"' -o consignment-service

# Copy application to scratch container
FROM scratch
COPY --from=builder /app/consignment-service /
CMD ["/consignment-service"]